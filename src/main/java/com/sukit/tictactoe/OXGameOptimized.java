/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Xenon
 */
public class OXGameOptimized {

    static int counter = 0;
    static Scanner read = new Scanner(System.in);
    static String[][] board = {
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"},};
    static String turn = "X";

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showBoard() {
        System.out.println("  1 2 3");
        for (int row = 0; row < board.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < board[row].length; col++) {
                System.out.print(" ");
                System.out.print(board[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(turn + " turn");
    }

    static void input() {
        //Normal flow
        boolean isValid = false;
        while (!isValid) {
            System.out.println("Please input Row Column: ");
            int inputRow = read.nextInt() - 1;
            int inputCol = read.nextInt() - 1;
//        System.out.println("row: " + inputRow + " col: " + inputCol);
            //put in board
            if (board[inputRow][inputCol] != "-") {
                System.out.println("The slot has taken!");
            } else {
                board[inputRow][inputCol] = turn;
                isValid = true;
                counter++;
            }
        }
    }

    static boolean checkHorizon() {
        /* Horizontal */
        String[] horizon = new String[3];
        horizon[0] = board[0][0] + board[0][1] + board[0][2];
        horizon[1] = board[1][0] + board[1][1] + board[1][2];
        horizon[2] = board[2][0] + board[2][1] + board[2][2];

        for (int i = 0; i < 3; i++) {
            if (horizon[i].equals("XXX") || horizon[i].equals("OOO")) {
                return false;
            }
        }
        return true;
    }

    static boolean checkVertical() {
        /* Vertical */
        String[] vertical = new String[3];
        vertical[0] = board[0][0] + board[1][0] + board[2][0];
        vertical[1] = board[0][1] + board[1][1] + board[2][1];
        vertical[2] = board[0][2] + board[1][2] + board[2][2];

        for (int i = 0; i < 3; i++) {
            if (vertical[i].equals("XXX") || vertical[i].equals("OOO")) {
                return false;
            }
        }
        return true;

    }
    
    static boolean checkDiagonal(){
        /* Diagonal */
        String[] diagonal = new String[2];
        diagonal[0] = board[0][0] + board[1][1] + board[2][2];
        diagonal[1] = board[0][2] + board[1][1] + board[2][0];

        for (int i = 0; i < 2; i++) {
            if (diagonal[i].equals("XXX") || diagonal[i].equals("OOO")) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWin() {
        if (counter > 8) {
            return false;
        }
        if(!checkHorizon()) {
            return false;
        }
        if(!checkVertical()){
            return false;
        }
        if(!checkDiagonal()){
            return false;
        }
        return true;
    }

    static void turnCycle() {
        if (turn.equals("X")) {
            turn = "O";
        } else {
            turn = "X";
        }
    }

    static void showResult() {
        if (counter > 8) {
            System.out.println("Draw!");
        } else {
            if (turn.equals("X")) {
                System.out.println("player O Win!");
            } else {
                System.out.println("player X Win!");
            }
        }
    }

    static void exit() {
        System.out.println("Bye Bye...");

    }

    public static void main(String[] args) {
        showWelcome();
        showBoard();
        do {
            showTurn();
            input();
            turnCycle();
            showBoard();
        } while (checkWin());
        showResult();
        exit();

    }
}
